

hotel::hotel(string nom,int costo,int num)
{
	numero_cuarto=num;
	tipo_cuarto=nom;
	estatus="Disponible";
	precio=costo;
	id_cliente_renta=" ";
}
/*
void hotel::mostrar()		//Funcion hecha para realizar pruebas
{
	cout<<"numero cuarto "<<numero_cuarto<<endl;
	cout<<"tipo cuarto"<<tipo_cuarto<<endl;
	cout<<"estatus "<<estatus<<endl;
}
*/

bool humano::verificarID(string idtocomparate, humano *array[], int tam){
	int flag = 0;
	for(int x=0;x<tam;x++){
		if(array[x]->id==idtocomparate){
			flag = 1;
		}
	}
	if(flag==0){
		return true;
	}else{
		return false;
	}
}
bool humano::agregar(humano *array[], int tam)
{
	string idingresado;
	cin.ignore(50,'\n');
	cout<<"Ingresa el codigo: ";
	cin>>idingresado;
	if(verificarID(idingresado, array, tam)){
		getline(cin,id);
		id=idingresado;
		cout<<"Ingresa el nombre: ";
		getline(cin,nombre);
		cout<<"Ingresa el domicilio: ";
		getline(cin,domicilio);
		cout<<"Ingresa el telefono: ";
		getline(cin,telefono);
		cout<<"Ingresa el RFC: ";
		getline(cin,RFC);
		return true;
	}else{
		cout<<"El ID ingresado ya existe."<<endl;
		system("pause");
		return false;
	}
}
const void humano::mostrar()
{
	
	cout<<"Nombre: "<<nombre<<endl;
	cout<<"Domicilio: "<<domicilio<<endl;
	cout<<"Telefono: "<<telefono<<endl;
	cout<<"RFC: "<<RFC<<endl;	
	cout<<"ID: "<<id<<endl;
}

int humano::buscar(string id_buscado)
{
	
	if(id==id_buscado)
	return 1;
	else
	return 2;
}
int empleados::verificar_empleados()//Aqui, asigno puesto, y devuelvo un 0 si se agrego correctamente a ese puesto o un -1 si ya se lleno ese puesto
{
	string aux;
	int opcion=0;
	cout<<"\nElija que puesto desempe�ara su nuevo empleado"<<endl;
    cout<<"1. Gerente "<<endl;
    cout<<"2. Administrador"<<endl;
    cout<<"3. Servicio"<<endl;
    do
	{
	cin>>opcion;
    switch(opcion)
    {
    case 1:
	aux="Gerente";
    puesto=aux;
	sueldo=25000;
	  opcion=0;
    break;
    case 2:
    aux="Administrador";
    puesto=aux;
    sueldo=8000;
      opcion=0;
    break;
    case 3:
    aux="Servicio";
    puesto=aux;
    sueldo=6000;
      opcion=0;
    break;
    default:
    cout<<"Losiento, esa opcion no existe"<<endl;
    break;
    }
    }while (opcion!=0);


	aux="Gerente";
	if(puesto==aux)
	{
		if(contador_empleados[0]<2)
		{
		contador_empleados[0]++;
		WR_Report("reporte_gerentes.txt");
		return 0;		
		}
		else
		{
		cout<<"Ya no se puede agregar otro gerente"<<endl;
		return -1;
		}
	}
	else
	{
	aux="Administrador";
	if(puesto==aux)
	{
		if(contador_empleados[1]<3)
		{
		contador_empleados[1]++;
		WR_Report("reporte_administradores.txt");
		return 0;	
		}
		else
		{
		cout<<"Ya no se puede agregar otro administrador"<<endl;
		return -1;
		}
	}
		else
		{
			aux="Servicio";
			if(puesto==aux)
			{
				if(contador_empleados[2]<5)
				{
				contador_empleados[2]++;
				WR_Report("reporte_servicio.txt");
				return 0;	
				}
					else
					{
					cout<<"Ya no se puede agregar otro empleado de servicio"<<endl;
					return -1;
					}
		   }
		}
	}

}
bool empleados::agregar(humano *array[], int tam)
{	
	int auxi;
	if(humano::agregar(array, tam)){
		do
		{
			auxi=empleados::verificar_empleados();
		}while(auxi==-1);						//Mientras reciba un error(el unico error que puede haber es que se ingrese un tipo de trabajador donde ya este lleno el cupo)
												//le pedira al usuario volver a validar el puesto, si ya se lleno la capacidad de empleados, esta funcion ya no se manda a llamar
												//Para evitar el bucle infinito
		WR_Report("reporte_empleados.txt");
		return true;
	}else{
		return false;
	}
}
const void empleados::mostrar()
{
	humano::mostrar();
	cout<<"Puesto: "<<puesto<<endl;
	cout<<"Sueldo: "<<sueldo<<endl;	
}

void empleados::eliminar()//Reduzco en 1, el puesto del contador del empleado a borrar, reutilizando codigo de validar empleado, obviamente.
{
	string aux;
	aux="Gerente";
	if(puesto==aux)
	{
		contador_empleados[0]--;	
	}
	else
	{
	aux="Administrador";
	if(puesto==aux)
	{
	contador_empleados[1]--;
	}
		else
		{
			aux="Servicio";
			if(puesto==aux)
			{
				contador_empleados[2]--;
		    }
		}
	}
	
}

const int empleados::checarPuestoAlto()
{
	if(contador_empleados[0]>0)
	return 1;
	else
	if(contador_empleados[1]>0)
	return 1;
	else
	return -1;
}

int buscar(string id_buscado, humano *p2[],int tamanoPresente)
{
	int encontrado=0,bandera=0;
			for(i=0;i<tamanoPresente;i++)
			{
			encontrado=p2[i]->buscar(id_buscado);
			if(encontrado==1)
			return i;
			}
			if (encontrado==2)
		   return -1;		
}

void eliminarEmpleado(int posicion,humano *p2[],int tamanoPresente)
{
	i=posicion;
	int x;
	reinterpret_cast< empleados * >(p2[posicion])->eliminar();//Como p2 es un puntero de clase humano, y no se puede acceder a un miembro de una clase derivada, la reinterpreto como si fuera              //de la clase empleado
	for(;i<tamanoPresente;i++)
	{
		x=i;
		p2[x]=p2[x+1];		//Aqui se reacomoda el arreglo, empezando desde donde hay que eliminar, hasta donde se tiene agregado
	}
}

bool cliente::agregar(humano *array[], int tam)
{	
	if(humano::agregar(array, tam)){
		cout<<"Ingresa el correo: ";
		getline(cin,correo);
		WR_Report("reporte_clientes.txt");
		return true;
	}else{
		return false;
	}	
		
}

const void cliente::mostrar()
{
	humano::mostrar();
	cout<<"Correo: "<<correo<<endl;
	if(cuarto_rentado!=-1)
	cout<<"Numero de habitacion rentada: "<<cuarto_rentado;
	else
	cout<<"Sin habitacion rentada";
}

void eliminarCliente(int posicion,humano *p2[],int &tamanoClientes)//Clase amiga de cliente y de humano
{
	i=posicion;
	if(reinterpret_cast< cliente * >(p2[i])->cuarto_rentado==-1)
	{
	for(;i<tamanoClientes;i++)
	{
		x=i;
		p2[x]=p2[x+1];		//Aqui se reacomoda el arreglo, empezando desde donde hay que eliminar, hasta donde se tiene agregado
	}
  	tamanoClientes--;
  	}
  	else
  	cout<<"No se puede eliminar el cliente "<<p2[i]->nombre<<" por que tiene el cuarto "<<reinterpret_cast< cliente * >(p2[i])->cuarto_rentado<<" rentado"<<endl;
}

int checarGente(humano *p2[],int tamanoClientes,int tamanoPresente)
{
	for(i=0;i<tamanoPresente;i++)
	{
	x=reinterpret_cast< empleados * >(p2[i])->checarPuestoAlto();
	}
	if(x==1 && tamanoClientes!=0)
	return 1;
	else
	return -1;
}

int hotel::rentar(string nom)
{
	if(nom==tipo_cuarto)
	{
		if(estatus=="Disponible")
		return 1;
		else
		return -1;
	}
	return 0;
}

int hotel::desrentar(int num_habit)
{
	if(estatus=="Rentado" && numero_cuarto==num_habit)
	return 1;
	else
	return -1;
}

void empleados::reajuste_sueldo(int costo)
{
	sueldo=(costo*.05)+sueldo;
}






void Renta_desrenta(humano *p[],humano *p2[],hotel *p3[],int tamanoPresente,int tamanoClientes,int eleccion)//Clase amiga de cliente, humano y hotel
{

	string aux,nom;
	int opcion,y=0,z=0,posicion_cuarto_rentado,posicion_empleado_arentar,posicion_cliente_arentar,a=0;
	string idempleadorentar;
	if(eleccion==4)
	{
		
        do
		{
		cin.ignore(50,'\n');
		cout<<"Porfavor ingrese el codigo del empleado que le hara la renta (Solo gerentes o administradores)"<<endl;
		getline(cin,idempleadorentar);													
		y=buscar(idempleadorentar,p2,tamanoPresente);		//Busco que exista el empleado
		if(y!=-1)	//Si si existe el empleado procedo
		{
			if(reinterpret_cast<empleados * >(p2[y])->puesto=="Servicio") //Si es de servicio, marco un error
			{
			system("cls");
			cout<<"Un empleado de servicio no tiene autoridad para rentar habitaciones";
			z=-1;
			}
			else
			z=0;	//Limpio la variable "error" si no es de servicio  por si ya fuera la 2da o 3era vez que se intenta
		}
		else		
		{
			system("cls");
			cout<<"Codigo de empleado invalido";
			z=-1;
		}

		}while(z==-1); //Mientras ocurra algun error de los mencionados arriba, se repetira infinitamente
		
	posicion_empleado_arentar=y; //Si todo salio bien, guardo la posicion del empleado
	
	cout<<"Ingresa la id del cliente a rentarle su habitacion: ";
	getline(cin,aux);
	x=buscar(aux,p,tamanoClientes); //Se revisa si el cliente existe
	if(x==-1) //Si no existe, se imprime en pantalla lo de abajo y se termina la funcion
	cout<<"Cliente inexsistente";
	else
	{
		if(reinterpret_cast< cliente * >(p[x])->cuarto_rentado==-1) //Se revisa que no tenga ya rentado un cuarto el cliente en cuestion
		{
		posicion_cliente_arentar=x;
		system("cls");
		cout<<"Buen dia, "<<p[x]->nombre<<endl;
		do
		{
		cout<<"Nuestras habitaciones son: "<<endl;	
		cout<<"1 Habitacion doble"<<endl;
		cout<<"2 Habitacion cuadruple"<<endl;
		cout<<"3 Habitacion de lujo"<<endl;
		cin>>opcion;
		switch(opcion)
		{
		case 1:
		nom="Habitacion doble";								
		for(i=0; i<10; i++)
		{
		y=p3[i]->rentar(nom);
		x=i;
		if(y==1)
		i=10;
		}
		if(y==-1)
		{
		cout<<"Ese tipo de habitacion no esta disponible mil disculpas";				//Si ya no hay disponible el tipo de habitacion elegida, se termina la funcion
		}
		break;
		case 2:
		nom="Habitacion cuadruple";
		for(i=0; i<10; i++)
		{
		y=p3[i]->rentar(nom);
		x=i;
		if(y==1)
		i=10;
		}
		if(y==-1)
		{
		cout<<"Ese tipo de habitacion no esta disponible mil disculpas";
		}
		break;
		case 3:
		nom="Habitacion de lujo";
		for(i=0; i<10; i++)
		{
		y=p3[i]->rentar(nom);
		x=i;
		if(y==1)
		i=10;
		}
		if(y==-1)
		{
		cout<<"Ese tipo de habitacion no esta disponible, mil disculpas";
		}
		break;
		
		}	
			
		}while(opcion<1 || opcion>=4);
		
		if(y!=-1)			//Si si esta disponible el tipo de habitacion elegido, se procede con la renta
		{
		posicion_cuarto_rentado=x; //Guardo la posicion del cuarto de habitacion que se rentara
		
		
		if(reinterpret_cast<empleados * >(p2[posicion_empleado_arentar])->puesto=="Administrador") //Si el empleado elegido es un administrador, se le a�ade el 5% de su sueldo
		reinterpret_cast<empleados * >(p2[posicion_empleado_arentar])->reajuste_sueldo(p3[posicion_cuarto_rentado]->precio);
			
		p3[posicion_cuarto_rentado]->estatus="Rentado"; //Cambio el estatus de la habitacion
		p3[posicion_cuarto_rentado]->id_cliente_renta=p[posicion_cliente_arentar]->id; //Cambio el estatus de la habitacion
		y=p3[posicion_cuarto_rentado]->numero_cuarto; //Guardo el numero de la habitacion del cliente
		reinterpret_cast<cliente * >(p[posicion_cliente_arentar])->cuarto_rentado=p3[posicion_cuarto_rentado]->numero_cuarto; //Le asigno la habitacion al cliente
		
		cout<<p3[posicion_cuarto_rentado]->tipo_cuarto<<" numero: "<<p3[posicion_cuarto_rentado]->numero_cuarto<<" rentada con exito"<<endl;
		}//Cierre de si si esta disponible el tipo de habitacion elegido, se procede con la renta
			
			

		
	}//Cierre de if, si el cliente que rentara no tiene cuarto rentado
	else
	cout<<"Usted ya tiene la habitacion numero:"<<reinterpret_cast< cliente * >(p[x])->cuarto_rentado<<" rentada";		
}//Cierre de else, cuando el cliente si existe en la renta
}//Cierre de if, que controla si es renta o desrenta
			else//Si se va a desrentar
			{
								do
								{
								cin.ignore(50,'\n');
								cout<<"Porfavor ingrese el codigo del empleado que le hara la renta (Solo gerentes o administradores)"<<endl;
								getline(cin,idempleadorentar);													
								y=buscar(idempleadorentar,p2,tamanoPresente);		//Busco que exista el empleado
								if(y!=-1)	//Si si existe el empleado procedo
								{
								if(reinterpret_cast<empleados * >(p2[y])->puesto=="Servicio" ||reinterpret_cast<empleados * >(p2[y])->puesto=="Administrador" ) //Si es de servicio, marco un error
								{
								cout<<"Solo gerentes pueden desrentar habitaciones";
								z=-1;
								}
								else
								z=0;	//Limpio la variable "error" si no es de servicio  por si ya fuera la 2da o 3era vez que se intenta
								}
								else		
								{
								system("cls");
								cout<<"Codigo de empleado invalido";
								z=-1;
								}
								}while(z==-1);
				posicion_empleado_arentar=y; //Si todo salio bien, guardo la posicion del empleado				
				cout<<"Ingresa la id del cliente que desalojara  su habitacion: ";
				getline(cin,aux);
				x=buscar(aux,p,tamanoClientes); //Se revisa si el cliente a desrentar existe
				if(x==-1) //Si no existe, se imprime en pantalla lo de abajo y se termina la funcion
				cout<<"Cliente inexsistente";
				else//Si el cliente a desrentar si existe...
				{
					if(reinterpret_cast< cliente * >(p[x])->cuarto_rentado!=-1) //Se revisa que si tenga cuarto a desrentar
					{
						posicion_cliente_arentar=x;
						a=reinterpret_cast< cliente * >(p[x])->cuarto_rentado;
						
								for(i=0; i<10; i++)
								{
								y=p3[i]->desrentar(a);
								z=i;
								if(y==1)
								i=10;
								}
								
								if(y!=-1)
								{		
								posicion_cuarto_rentado=z;			
								p3[posicion_cuarto_rentado]->estatus="Disponible";
								p3[posicion_cuarto_rentado]->id_cliente_renta=" ";
								reinterpret_cast< cliente * >(p[posicion_cliente_arentar])->cuarto_rentado=-1;
								}
				
				
				
				
					}
					else//Cierre de se revisa que si tenga cuarto a desrentar
					cout<<"Usted no tiene habitacion a desalojar";
				
				
				
				
				}//Cierre de si el cliente a desrentar si existe....
				
				
				
				
				
				
				
				
				
			}//Cierre de si se va a desrentar
}//Cierre de funcion









void humano::WR_Report(char nArch[]){
	ofstream escritura(nArch, ios::app);
	if(escritura){
		escritura<<"ID:		"<<id<<endl;
		escritura<<"Nombre:		"<<nombre<<endl;
		escritura<<"Domicilio:	"<<domicilio<<endl;
		escritura<<"Tel�fono:	"<<telefono<<endl;
		escritura<<"RFC:		"<<RFC<<endl;
	}else{
		cout<<"ERROR"<<endl;
	}
	
	escritura.close();
}
void cliente::WR_Report(char nArch[]){
	humano::WR_Report(nArch);
	ofstream escritura(nArch, ios::app);
	if(escritura){
		escritura<<"Correo:		"<<correo<<endl;
		if(cuarto_rentado!=-1){
			escritura<<"Cuarto rentado: "<<cuarto_rentado<<endl;
		}
		escritura<<"--------------------------------"<<endl;
	}else{
		cout<<"ERROR"<<endl;
	}
	
	escritura.close();
}

void empleados::WR_Report(char nArch[]){
	humano::WR_Report(nArch);
	ofstream escritura(nArch, ios::app);
	if(escritura){
		escritura<<"Puesto:		"<<puesto<<endl;
		escritura<<"Sueldo:		"<<sueldo<<endl;
		escritura<<"--------------------------------"<<endl;
	}else{
		cout<<"ERROR"<<endl;
	}
	
	escritura.close();
}

void RD_report(char nArch[]){
	char c;
	ifstream lectura(nArch);
	while(lectura.get(c)){
		cout<<c;
	}
	lectura.close();
}

void updateRoomReport(hotel *cuartos[], humano *clientes[], int tamCli){							//Actualiza los reportes de las habitaciones disponibles y no disponibles.
	int status, flag=0;
	//-----------------Se borran los datos actuales de los archivos
	ofstream escritura("habitaciones_disponibles.txt");
	if(escritura){
		flag++;
	}else{
		cout<<"ERROR"<<endl;
	}
	escritura.close();
	ofstream escritura1("habitaciones_nodisponibles.txt");
	if(escritura1){
		flag++;
	}else{
		cout<<"ERROR"<<endl;
	}
	escritura1.close();
	ofstream escritura2("reporte_habitaciones.txt");
	if(escritura2){
		flag++;
	}else{
		cout<<"ERROR"<<endl;
	}
	escritura2.close();
	ofstream escritura3("reporte_clientes.txt");
	if(escritura3){
		flag++;
	}else{
		cout<<"ERROR"<<endl;
	}
	escritura3.close();
	//--------------------------------------------------------------
	if(flag==4){
		for(int x=0;x<10;x++){
			status = cuartos[x]->getStatus();							//Obtengo el status de la habitaci�n.
			if(status==1){
				cuartos[x]->WR_Report("habitaciones_disponibles.txt");
			}else if(status==-1){
				cuartos[x]->WR_Report("habitaciones_nodisponibles.txt");
			}
			cuartos[x]->WR_Report("reporte_habitaciones.txt");
		}
		for(int x=0;x<tamCli;x++){
			clientes[x]->WR_Report("reporte_clientes.txt");
		}
	}
}

void updateEmpleadosReport(humano *empleados[], int tamEmp){							//Actualiza los reportes de las habitaciones disponibles y no disponibles.
	int status, flag=0;
	//-----------------Se borran los datos actuales del archivo
	ofstream escritura("reporte_empleados.txt");
	if(escritura){
		flag++;
	}else{
		cout<<"ERROR"<<endl;
	}
	escritura.close();
	//--------------------------------------------------------------
	if(flag==1){
		for(int x=0;x<tamEmp;x++){
			empleados[x]->WR_Report("reporte_empleados.txt");
		}
	}
}

void hotel::WR_Report(char nArch[]){ 
	ofstream escritura(nArch, ios::app);
	if(escritura){
		escritura<<"No. Cuarto:			"<<numero_cuarto<<endl;
		escritura<<"Tipo de Cuarto:			"<<tipo_cuarto<<endl;
		escritura<<"Estatus:			"<<estatus<<endl;
		escritura<<"Precio:				"<<precio<<endl;
		if(id_cliente_renta!=" "){
			escritura<<"Rentado por:			"<<id_cliente_renta<<endl;
		}
		escritura<<"--------------------------------"<<endl;
	}else{
		cout<<"ERROR"<<endl;
	}
	escritura.close();
}

int hotel::getStatus(){
	if(estatus=="Disponible"){
		return 1;
	}else if(estatus=="Rentado"){
		return -1;
	}
}

void refreshfiles(){
	ofstream escritura("habitaciones_disponibles.txt");
	escritura.close();
	
	escritura.open("habitaciones_nodisponibles.txt");
	escritura.close();
	
	escritura.open("reporte_habitaciones.txt");
	escritura.close();
	
	escritura.open("reporte_clientes.txt");
	escritura.close();
	
	escritura.open("reporte_empleados.txt");
	escritura.close();
	
	escritura.open("reporte_gerentes.txt");
	escritura.close();
	
	escritura.open("reporte_administradores.txt");
	escritura.close();
	
	escritura.open("reporte_servicio.txt");
	escritura.close();
}
