int i=0; //Variable global que sirve para la mayoria de los for
int x=0; //otro auxiliar recurrente
class hotel;

class humano
{
string nombre;
string domicilio;
string telefono;
string RFC;
string id;
public:	
humano(string nom,string dom,string tel,string rf, string id_emp)
{
	nombre=nom;
	domicilio=dom;
	telefono=tel;
	RFC=rf;
	id=id_emp;
}
virtual bool agregar(humano *array[], int tam);
virtual const void mostrar()=0;
virtual void WR_Report(char nArch[]);	
int buscar(string);
bool verificarID(string idtocomparate, humano *array[], int tam);
friend void Renta_desrenta(humano *[],humano*[],hotel*[],int,int,int);
friend void  eliminarCliente(int posicion,humano *p2[],int &tamanoClientes);
 virtual ~humano(){
};
};
class cliente:public humano
{
string correo;
int cuarto_rentado; //Para saber que empleado ha rentado que cuarto
public:
bool agregar(humano *array[], int tam);
const void mostrar();	
cliente(string nom,string dom,string tel,string rf,string mail,string id):humano(nom,dom,tel,rf, id){
	correo = mail;
	cuarto_rentado=-1;
}
friend void Renta_desrenta(humano *[],humano*[],hotel*[],int,int,int);
friend void  eliminarCliente(int posicion,humano *p2[],int &tamanoClientes);
void WR_Report(char nArch[]);
~cliente(){};	
};

class empleados:public humano
{
string puesto;
float sueldo;
int static contador_empleados[3];//variable para saber cuantos empleados tenemos posicion 0 para gerentes, 1 para administradores y 2 para empleados de servicio
public:
bool agregar(humano *array[], int tam);
const void mostrar();
 int verificar_empleados();
void eliminar();
empleados(string nom,string dom,string tel,string rf,string pue,string id, float suel):humano(nom,dom,tel,rf, id)
{
	puesto=pue;
	sueldo = suel;
} //constructor con parametros, para que el programa empieze con un empleado de cada tipo
const int checarPuestoAlto();
void reajuste_sueldo(int costo);
friend void Renta_desrenta(humano *[],humano*[],hotel*[],int,int,int);
void WR_Report(char nArch[]);
~empleados(){};
};
int empleados::contador_empleados[] = {1, 1, 1}; //inicializamos el contador de empleados, para llevar el control de cuantos empleados llevamos de cada tipo

class hotel
{
	int numero_cuarto;
	string tipo_cuarto;
	string estatus;
	string id_cliente_renta;
	int precio;
	public:
	hotel(string,int,int);
	int rentar(string);
	int desrentar(int);
	int getStatus();
	void WR_Report(char nArch[]);
	//void mostrar();
	friend void Renta_desrenta(humano *[],humano*[],hotel*[],int,int,int);
	~hotel(){};
};

